FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-dev python3-pip build-essential
COPY . /app
WORKDIR /app 
RUN pip3 install -r requirements.txt
CMD ["python3","flask_server.py"]
CMD gunicorn  flask_server:app


